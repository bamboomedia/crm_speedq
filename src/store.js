import Vue from 'vue'
import Vuex from 'vuex'
import user from './store/user'
import contact from './store/contact'
import region from './store/region'
import group from './store/group'
import message from './store/message'
import dashboard from './store/dashboard'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    barImage: 'https://demos.creative-tim.com/material-dashboard/assets/img/sidebar-1.jpg',
    drawer: null,
  },
  mutations: {
    SET_BAR_IMAGE (state, payload) {
      state.barImage = payload
    },
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
  },
  actions: {

  },
  
  modules: {
    user: {
      namespaced: true,
      ...user
    },
    contact: {
      namespaced: true,
      ...contact
    },
    region: {
      namespaced: true,
      ...region
    },
    group: {
      namespaced: true,
      ...group
    },
    message: {
      namespaced: true,
      ...message
    },
    dashboard: {
      namespaced: true,
      ...dashboard
    }
  }
})
