import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  mode:'history',
  routes: [
    {
      name: 'Login',
      path: '/login',
      component : () => import('@/views/dashboard/Login'),
      meta: {notRequireAuth: true}
    },
    {
      path: '/',
      component: () => import('@/views/dashboard/Home'),
      children: [
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        {
          name: 'User CRM',
          path: 'pages/usercrm',
          component: () => import('@/views/dashboard/pages/Usercrm'),
        },
        {
          name: 'Group',
          path: 'pages/group',
          component: () => import('@/views/dashboard/pages/Grouplist'),
        },
        {
          name: 'Contact',
          path: 'pages/contact',
          component: () => import('@/views/dashboard/pages/Contactlist'),
        },
        {
          name: 'Message',
          path: 'pages/messagetemplate',
          component: () => import('@/views/dashboard/pages/Messagetemplate'),
        },
        {
          name: 'Send Message',
          path: 'schedule/send',
          component: () => import('@/views/dashboard/pages/schedule/Send'),
        },
        {
          name: 'Schedule',
          path: 'schedule/schedule',
          component: () => import('@/views/dashboard/pages/schedule/Schedule'),
        },
        {
          name: 'Monitor',
          path: 'schedule/monitor',
          component: () => import('@/views/dashboard/pages/schedule/Monitor'),
        },
      ],
    },
  ],
})

router.beforeEach((to, from, next) => {

  const tokenExist = localStorage.getItem('crm')
  const userData = JSON.parse(localStorage.getItem('user'))
  if (to.path === '/login' && tokenExist && userData)
    next('/')

  if(to.meta.notRequireAuth)
    return next()

  if (tokenExist && userData)
    next()
  else
    next('/login')
})

export default router
