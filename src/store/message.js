import Api from './Api'
import moment from 'moment'

const state = () => ({
  template: [],
  templateDet: {},
  directMessage: [],
  schedule: [],
  scheduleDet: [],
  
})

const mutations = {
  SET_TEMPLATE(state, value) {
    state.template = value
  },
  SET_TEMPLATE_DETAIL(state, value) {
    state.templateDet = value
  },
  SET_DIRECT_MESSAGE(state, value) {
    state.directMessage = value
  },
  SET_SCHEDULE(state, value) {
    state.schedule = value
  },
  SET_SCHEDULE_DETAIL(state, value) {
    state.scheduleDet = value
  },
}

const actions = {
  async GET_TEMPLATE({
    commit
  }, data = false) {
    let res = !data ? await Api.get('/message/template') : await Api.get('/message/template', {params: data})
    res.data.data.map(x => x.active = x.active ? 'Active' : 'Inactive')
    commit('SET_TEMPLATE', res.data.data)
    return Promise.resolve(res)
  },
  async GET_SCHEDULE({
    commit
  }, data = false) {
    const typeSchedule = ['', 'Every Day', 'Every Week', 'Every Month', 'Every Year', 'Birthday']
    const recipientSchedule = ['Contact', 'Group', 'All']
    let res = !data ? await Api.get('/message/schedule') : await Api.get('/message/schedule', { params: data })
    res.data.data.map(x =>  {
      x.status = x.active ? 'Active' : 'Inactive'
      x.type = typeSchedule[x.type]
      x.recipient = recipientSchedule[x.recipient]
      x.start_date = moment(x.start_date).format('LL')
      x.end_date = moment(x.end_date).format('LL')
    })
    commit('SET_SCHEDULE', res.data.data)
    return Promise.resolve(res)
  },
  async GET_DIRECT_MSG({
    commit
  }, data = false) {
    let res = !data ? await Api.get('/message/send') : await Api.get('/message/send', {
      params: data
    })
    res.data.data.map(x => x.date = moment(x.date).format('LL'))
    commit('SET_DIRECT_MESSAGE', res.data.data)
    return Promise.resolve(res)
  },
  async GET_DETAIL({
    commit
  }, id) {
    let res = await Api.get(`/message/template/${id}`)
    commit('SET_TEMPLATE_DETAIL', res.data.data)
    return Promise.resolve(res)
  },
  async GET_SCHEDULE_DETAIL({
    commit
  }, id) {
    let res = await Api.get(`/message/schedule/${id}`)
    
    commit('SET_SCHEDULE_DETAIL', res.data.data)
    return Promise.resolve(res)
  },
  async DELETE_TEMPLATE({}, body) {
    let res = await Api.delete(`/message/template/${body}`)
    return Promise.resolve(res)
  },
  async DELETE_MESSAGE({}, body) {
    let res = await Api.delete(`/message/send/${body}`)
    return Promise.resolve(res)
  },
  async DELETE_SCHEDULE({}, body) {
    let res = await Api.delete(`/message/schedule/${body}`)
    return Promise.resolve(res)
  },
}

export default {
  state,
  mutations,
  actions
}