import axios from 'axios'

const instance = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL
})
  
instance.interceptors.request.use(
  (req) => {
    const token = localStorage.getItem('crm')
    req.headers.Authorization = token ? 'Bearer ' + token : ''
    return req
  },
  undefined
)

instance.interceptors.response.use(
  undefined,
  (error) => {
    if (error.response && error.response.status === 401) {
      localStorage.removeItem('crm')
      localStorage.removeItem('user')
      window.location.reload()
    }
  }
)

export default instance