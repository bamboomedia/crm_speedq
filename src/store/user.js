import Api from './Api'

const state = () => ({
  user: [],
})

const mutations = {
  SET_USER(state, value) {
    state.user = value
  },
}

const actions = {
  async GET_USER({
    commit
  }) {
    let res = await Api.get('/user')
    res.data.data.map(x => x.active = x.active ? 'Active' : 'Inactive')
    commit('SET_USER', res.data.data)
    return Promise.resolve(res)
  },
  async ADD_USER({}, body) {
    let res = await Api.post('/user', body)
    return Promise.resolve(res)
  },
  async DELETE_USER({}, body) {
    let res = await Api.delete(`/user/${body}`)
    return Promise.resolve(res)
  },
  async EDIT_USER({}, body) {
    let res = await Api.put(`/user/${body.username}`, body)
    return Promise.resolve(res)
  },
}

export default {
  state, mutations, actions
}