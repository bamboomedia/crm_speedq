import axios from 'axios'
import qs from 'querystring'

const state = () => ({
  prov: [],
  kab: [],
})

const mutations = {
  SET_PROV(state, value) {
    state.prov = value
  },
  SET_KAB(state, value) {
    state.kab = value
  },
}

const actions = {
  async GET_PROV({ commit },) {
    let res = await axios({
      method: 'post',
      url: 'https://smartcitypanel.isn-speed.com/api/region/prov.php',
      data: qs.stringify({
        negaraId: 62
      }),
      headers: {
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
      }
    })
    let data1 = res.data.data.map(row => ({
      text: row.prov,
      value: parseInt(row.id_prov)
    }))
    commit('SET_PROV', data1)
  },
  async GET_KAB({ commit }, provid) {
     let res = await axios({
       method: 'post',
       url: 'https://smartcitypanel.isn-speed.com/api/region/kab.php',
       data: qs.stringify({
         provId: provid
       }),
       headers: {
         'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
       }
     })
      let data1 = res.data.data.map(row => ({
        text: row.kab,
        value: parseInt(row.id_kab)
      }))
    commit('SET_KAB', data1)
  },
}

export default {
  state,
  mutations,
  actions
}