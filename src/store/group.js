import Api from './Api'

const state = () => ({
  group: [],
})

const mutations = {
  SET_GROUP(state, value) {
    state.group = value
  },
}

const actions = {
  async GET_GROUP({
    commit
  }, data) {
    let res = await Api.get('/group', {params: data})
    res.data.data.map(x => {
      x.contact_total = x.contact.length
      x.status = x.active ? 'Active' : 'Inactive'
    })
    commit('SET_GROUP', res.data.data)
    return Promise.resolve(res)
  },
//   async ADD_USER({}, body) {
//     let res = await Api.post('/user', body)
//     return Promise.resolve(res)
//   },
  async DELETE_GROUP({}, body) {
    let res = await Api.delete(`/group/${body}`)
    return Promise.resolve(res)
  },
//   async EDIT_USER({}, body) {
//     let res = await Api.put(`/user/${body.username}`, body)
//     return Promise.resolve(res)
//   },
}

export default {
  state,
  mutations,
  actions
}