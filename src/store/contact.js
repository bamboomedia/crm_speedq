import Api from './Api'
import moment from 'moment'

const state = () => ({
  contact: [],
  contactDet: {},
  profession:[]
})

const mutations = {
  SET_CONTACT(state, value) {
    state.contact = value
  },
  SET_CONTACT_DET(state, value){
    state.contactDet = value
  },
  SET_PROFESSION(state, value){
    state.profession = value
  }
}

const actions = {
  async GET_CONTACT({
      commit
    }, data) {

    const opts = {
      headers: {
        ['Cache-Control']: 'no-cache'
      },
      params: data
    }

    // let res = !data ? await Api.get('/contact', opts) : await Api.get('/contact', {
    //   params: data, ...opts
    // })
    const res = await Api.get('/contact', opts)
    res.data.data.map((row) => (row.ttl = `${row.place_of_birth}, ${moment(row.date_of_birth).format('LL')}`))
    commit('SET_CONTACT', res.data.data)
    return Promise.resolve(res)
  },
  async GET_DETAIL({commit}, id) {
    let res = await Api.get(`/contact/detail/${id}`)
    const momen = moment(res.data.data.date_of_birth).format('L')
    // res.data.data.date_of_birth = moment(res.data.data.date_of_birth).format('L')
    res.data.data.date_of_birth = momen == 'Invalid date' ? '0000-00-00' : momen
    // console.log(res.data.data.date_of_birth)
    // res.data.data.date_of_birth  == 'Invalid date' ? moment().format('YYYY-MM-DD') : moment(this.form.date_of_birth).format('YYYY-MM-DD')
    commit('SET_CONTACT_DET', res.data.data)
    return Promise.resolve(res)
  },
  async GET_PROFESSION({
    commit
  }) {
    let res = await Api.get('/profession')
    res.data.data.map(x => {
      x.text = x.name
      x.value = x.id
    })
    commit('SET_PROFESSION', res.data.data)
    return Promise.resolve(res)
  },
  async EDIT_CONTACT({}, body) {
    let res = await Api.put(`/contact/${body.id}`, body)
    return Promise.resolve(res)
  },
}

export default {
  state,
  mutations,
  actions
}