import Api from './Api'

const state = () => ({
  dashboard: [],
  dashboardProgram: [],
  dashboardDetail: [],
  programList: []
})

const mutations = {
  SET_DASHBOARD(state, value) {
    state.dashboard = value
  },
  SET_DASHBOARD_PROGRAM(state, value) {
    state.dashboardProgram = value
  },
  SET_DASHBOARD_DETAIL(state, value) {
    state.dashboardDetail = value
  },
  SET_PROGRAM_LIST(state, value) {
    state.programList = value
  },
}

const actions = {
  async GET_DASHBOARD({
    commit
  }) {

    let res = await Promise.all([Api.get('/dashboard'), Api.get('/dashboard/program')])
    commit('SET_DASHBOARD', res[0].data.data)
    commit('SET_PROGRAM_LIST', res[1].data.data.program_list.map(x => ({
      text: x.name,
      value: x.id
    })))
    // delete res[1].data.data.program_list
    // res[1].data.data.responden.map(x => x.status = x.status ? 'View' : 'Unview')
    // commit('SET_DASHBOARD_PROGRAM', res[1].data.data)

    return Promise.resolve(res)
  },
  async GET_DASHBOARD_DETAIL({
    commit
  },id) {
    let res = await Api.get(`/dashboard/program/${id}`)
    res.data.data.responden.map(x => x.status = x.status ? 'View' : 'Unview')
    commit('SET_DASHBOARD_PROGRAM', res.data.data)
    return Promise.resolve(res)
  },
}

export default {
  state,
  mutations,
  actions
}