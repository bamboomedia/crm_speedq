module.exports = {
  apps : [{
    name: 'frontend',
    script: 'serve -s ./dist -l 8080',
    instances: 1,
    autorestart: true,
    watch: true,
  }]
};
